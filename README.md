# TearTime Mat Editor
Some especial #D printer material editor

## TearTime material editor 
Editor with extended abilities for TierTime UP Studio.
Supporting files version 2.6.20.627.
Editor can work with settings for 3d printers with above version:
- UP 300
- UP BOX
- UP BOX+
- UP Cetus
- UP Plus
- UP Plus 2
- UP Plus 3
- UP X5
- UP mini
- UP mini2
Main function of this editor is abbilance to print with any accessible diameter extruder on all supported 3d printers.
For this recommended create settings for default 3d printer with required extruder diameter. After that rename 3d printer with this editor. 
**All changes makes at one's own risk.**


## Редактор материалов TearTime
Редактор с расширенными возможностями для TierTime UP Studio.
Написан для работы с файлами версии 2.6.20.627.
Поддерживает редактирование настроек материалов для 3d принтеров в указанной версии:
- UP 300
- UP BOX
- UP BOX+
- UP Cetus
- UP Plus
- UP Plus 2
- UP Plus 3
- UP X5
- UP mini
- UP mini2

Основная задача, которую решает данная программа - возможность печатать соплом любого разумного диаметра на всех доступных 3d принтерах. 
Рекомендуется создать для принтера по умолчанию набор настроек  в стандартном редакторе, выбрав нужный диаметр сопла, а затем с помощью данного редактора изменить тип принтера.
**Изменение любых настроек делается на свой страх и риск.**
