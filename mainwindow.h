#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTreeWidgetItem>
#include <QHash>
#include <QStandardPaths>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:

    void on_Load_Button_clicked();

    void on_Save_Button_clicked();

    void on_Apply_Button_clicked();

    void on_Data_tree_currentItemChanged(QTreeWidgetItem *current, QTreeWidgetItem *previous);

    void on_Load_def_user_Button_clicked();

    void on_Load_def_vendor_Button_clicked();

private:
    Ui::MainWindow *ui;

    std::vector <QJsonObject> json_obj_vector;
    QHash <QString, QLineEdit *> printer_lineedit_holder;
    QHash <QString, QLineEdit *> material_lineedit_holder;

    void tree_widget_filler();
    void ui_editor_page_filler();
    void load_json_data_from_file();

    QString sort_name_top = "p1";
    QString sort_name_mid = "p2";
    QString sort_name_bot = "p3";
    QString default_user_file = QString("%1/AppData/Roaming/Tiertime/DB/user.fmd").arg( QStandardPaths::standardLocations( QStandardPaths::HomeLocation ).at(0) );
    QString default_vendor_file =  QString("%1/AppData/Roaming/Tiertime/DB/vendor.fmd").arg( QStandardPaths::standardLocations( QStandardPaths::HomeLocation ).at(0) );
    const uint8_t amount_rotated_files = 5;
    QString current_data_file;

    QTreeWidgetItem * current_treewidget_item;

    enum ColumIndex
    {
      JsonVectorIndex = 1,
      ArrayIndex,
      GroupArrayIndex
    };
    QHash <QString, QString> group_name_holder =
    {
        {"p1", "printer_type" },
        {"p10", "Peel Ratio" },
        {"p11.0", "Line Width Outline" },
        {"p11.1", "Line Width Infill" },
        {"p11.2", "Line Width Support" },
        {"p12.0", "Scan Speed Outline" },
        {"p12.1", "Scan Speed Infill" },
        {"p12.2", "Scan Speed Support" },
        {"p13.0", "Send Ratio Outline" },
        {"p13.1", "Send Ratio Infill" },
        {"p13.2", "Send Ratio Support" },
        {"p14.0", "Temp Bias Outline °C" },
        {"p14.1", "Temp Bias Infill °C" },
        {"p14.2", "Temp Bias Support °C" },
        {"p15.0", "p15.0" },
        {"p15.1", "p15.1" },
        {"p15.2", "p15.2" },
        {"p16", "Overlap" },
        {"p17", "p17" },
        {"p18", "p18" },
        {"p19", "p19" },
        {"p2", "nozzle_diameter" },
        {"p20", "p20" },
        {"p21", "p21" },
        {"p22", "Joint size" },
        {"p23", "p23" },
        {"p24", "p24" },
        {"p25", "p25" },
        {"p26", "p26" },
        {"p27", "p27" },
        {"p28", "p28" },
        {"p3", "layer_thinckness" },
        {"p4", "quality" },
        {"p5", "p5" },                  //quality ... wtf mb stick_hatch adhesion
        {"p6", "Basic Temp" },
        {"p7", "plat_temprature_bias" },
        {"p8", "loop_temprature_bias" },
        {"p9", "Withdraw" }
    };
    QHash <QString, QString> material_name_holder =
    {
        {"b1", "some like id" },
        {"b2", "Material Name" },
        {"b3", "Material Manufact" },
        {"b4", "Print Temperature °C" },
        {"b5", "b5" },
        {"b6", "b6" },
        {"b7", "Material Diameter" },
        {"b8", "Density" },
        {"b9.0", "Shrinkage X" },
        {"b9.1", "Shrinkage Y" },
        {"b9.2", "Shrinkage Z" },
        {"b10", "b10" },
        {"b11", "b11" },
        {"b12", "b12" },
        {"b13", "b13" },
        {"b14", "b14" },
      //{"group", "" },
        {"m1", "m1" },
        {"m2", "m2" }
    };
    QHash <QString, QString> printer_name_holder =
    {
        {"-1",      "Default" },
        {"10104",   "" },
        {"10105",   "" },
        {"10111",   "" },
        {"10112",   "" },
        {"10114",   "" },
        {"10115",   "" }
    };
    QHash <QString, QString> qulity_name_holder =
    {
        {"0", "Normal" },
        {"1", "Fine" },
        {"2", "Fast" },
        {"3", "Turbo" },
        {"4", "Custom1" },
        {"5", "Custom2" }
    };
    QHash <QString, QPair<double, double>> min_max_group_validator_holder =
    {
//        {"p1", "printer_type" },
        {"p10", qMakePair(0.0, 100.0) }, // [0 - 100]
//        {"p11.0", "Line Width Outline" },
//        {"p11.1", "Line Width Infill" },
//        {"p11.2", "Line Width Support" },
//        {"p12.0", "Scan Speed Outline" },
//        {"p12.1", "Scan Speed Infill" },
//        {"p12.2", "Scan Speed Support" },
//        {"p13.0", "Send Ratio Outline" },
//        {"p13.1", "Send Ratio Infill" },
//        {"p13.2", "Send Ratio Support" },
        {"p14.0", qMakePair(0.0, 1000) },
        {"p14.1", qMakePair(0.0, 1000) },
        {"p14.2", qMakePair(0.0, 1000) },
//        {"p15.0", "p15.0" },
//        {"p15.1", "p15.1" },
//        {"p15.2", "p15.2" },
//        {"p16", "p16" },
//        {"p17", "p17" },
//        {"p18", "p18" },
//        {"p19", "p19" },
        {"p2", qMakePair(0.0, 3.5) },
//        {"p20", "p20" },
//        {"p21", "p21" },
//        {"p22", "p22" },
//        {"p23", "p23" },
//        {"p24", "p24" },
//        {"p25", "p25" },
//        {"p26", "p26" },
//        {"p27", "p27" },
//        {"p28", "p28" },
        {"p3", qMakePair(0.0, 10) },
        {"p4", qMakePair(0.0, 5) },
//        {"p5", "p5" },                  //quality ... wtf mb stick_hatch adhesion
        {"p6", qMakePair(0.0, 1000) },
        {"p7", qMakePair(0.0, 1000) },
        {"p8", qMakePair(0.0, 1000) },
//        {"p9", "Withdraw" }
    };
    QHash <QString, QPair<double, double>> min_max_material_validator_holder =
    {
        {"b1", qMakePair(0.0, 1e100) },
        {"b4", qMakePair(0.0, 1000) },
        {"b5", qMakePair(0.0, 1000) },
        {"b6", qMakePair(0.0, 1000) },
        {"b7", qMakePair(0.0, 10) },
        {"b8", qMakePair(0.0, 1000) },
        {"b9.0", qMakePair(0.0, 1000) },
        {"b9.1", qMakePair(0.0, 1000) },
        {"b9.2", qMakePair(0.0, 1000) },
        {"b10", qMakePair(0.0, 1000) },
        {"b11", qMakePair(0.0, 1000) },
        {"b12", qMakePair(0.0, 1000) },
        {"b13", qMakePair(0.0, 1000) },
        {"b14", qMakePair(0.0, 1000) },
        {"m2", qMakePair(0.0, 1e100) }
    };
};

#endif // MAINWINDOW_H
