#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "misc.h"

#include <QtWidgets>
#include "QtSql/QSqlDatabase"
#include "QSqlQuery"
#include "QSqlRecord"
#include <QDebug>
#include <QFile>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QtConcurrent>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowTitle(tr("TearTime Mat Editor"));

    ui->tabs->setCurrentWidget( ui->default_page );

    ui_editor_page_filler();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::tree_widget_filler()
{
    ui->Data_tree->clear();    
    QList<QTreeWidgetItem *> tree_items;

    for (size_t json_obj_cntr = 0; json_obj_cntr < json_obj_vector.size(); ++json_obj_cntr)
    {
        QJsonObject json_obj = json_obj_vector[json_obj_cntr];
        tree_items.append( new QTreeWidgetItem( (QTreeWidget*)0, QStringList(
                                                {   QString("%1").arg(json_obj.value("b2").toString()),
                                                    QString("%1").arg( json_obj_cntr )
                                                }                            ) ) );

        QJsonArray jsn_arr = json_obj.value("group").toArray();

        QHash< QString, QHash< QString, QHash< QString, std::vector < int > > > > data;

        for ( int elem_counter = 0; elem_counter <  jsn_arr.size(); ++elem_counter )
        {
        data[ QString::number( jsn_arr[elem_counter].toObject()[ sort_name_top ].toDouble() ) ]
            [ QString::number( jsn_arr[elem_counter].toObject()[ sort_name_mid ].toDouble() ) ]
            [ QString::number( jsn_arr[elem_counter].toObject()[ sort_name_bot ].toDouble() ) ].push_back( elem_counter );

        }

        for (QString str1 : data.uniqueKeys())
        {
            QTreeWidgetItem * item1 = new QTreeWidgetItem( static_cast<QTreeWidget *>(nullptr), QStringList( QString("%1").arg(str1) ) );
            for ( QString str2 : data[str1].uniqueKeys() )
            {

                QTreeWidgetItem * item2 = new QTreeWidgetItem( static_cast<QTreeWidget *>(nullptr), QStringList( QString("%1").arg(str2) ) );
                for ( QString str3 : data[str1][str2].uniqueKeys() )
                {
                    QTreeWidgetItem * item3 = new QTreeWidgetItem( static_cast<QTreeWidget *>(nullptr), QStringList( QString("%1").arg(str3) ) );

                    for ( size_t arr_cntr = 0; arr_cntr <  data[str1][str2][str3].size(); ++arr_cntr )
                    {
                        item3->addChild( new QTreeWidgetItem( static_cast<QTreeWidget *>(nullptr), QStringList(
                                            {  QString("%1").arg( jsn_arr[data[str1][str2][str3][arr_cntr]].toObject()["p4"].toDouble() ),
                                               QString("%1").arg( json_obj_cntr ),
                                               QString("%1").arg( arr_cntr ),
                                               QString("%1").arg( data[str1][str2][str3][arr_cntr] )
                                            }                                                                  ) ) );
                    }
                    item2->addChild( item3 );

                }
                item1->addChild( item2 );
            }
            tree_items.last()->addChild( item1 );
        }
    }

    ui->Data_tree->insertTopLevelItems(0, tree_items);
    ui->Data_tree->sortItems(0, Qt::AscendingOrder);

    current_treewidget_item = nullptr;
    ui->tabs->setCurrentWidget( ui->default_page );

}

void MainWindow::ui_editor_page_filler()
{

    int data_cnt = 0;

    QList<QString> sorted_list = group_name_holder.uniqueKeys();
    std::sort(sorted_list.begin(), sorted_list.end());

    for ( QString key : sorted_list )
    {
        QLabel * label = new QLabel( group_name_holder.value(key) );
        QLineEdit * lineedit = new QLineEdit();
        printer_lineedit_holder.insert(key, lineedit);        
        if ( data_cnt < group_name_holder.size()/2 )
            ui->printer_form_l->addRow(label, lineedit);
        else
            ui->printer_form_r->addRow(label, lineedit);

        CustomDoubleValidator * validator = new CustomDoubleValidator;
        if ( min_max_group_validator_holder.contains(key) )
        {
            validator->setBottom( min_max_group_validator_holder[key].first );
            validator->setTop( min_max_group_validator_holder[key].second );
        }
        lineedit->setValidator(validator);

        data_cnt++;
    }

    sorted_list = material_name_holder.uniqueKeys();
    std::sort(sorted_list.begin(), sorted_list.end());

    for ( QString key : sorted_list )
    {
        QLabel * label = new QLabel( material_name_holder.value(key) );
        QLineEdit * lineedit = new QLineEdit();
        material_lineedit_holder.insert(key, lineedit);
        ui->material_form->addRow(label, lineedit);

        if ( min_max_material_validator_holder.contains(key) )
        {
            CustomDoubleValidator * validator = new CustomDoubleValidator;
            validator->setBottom( min_max_material_validator_holder[key].first );
            validator->setTop( min_max_material_validator_holder[key].second );
            lineedit->setValidator(validator);
        }
    }

    ui->Data_tree->setColumnCount(4);
    ui->Data_tree->header()->hideSection(1);
    ui->Data_tree->header()->hideSection(2);
    ui->Data_tree->header()->hideSection(3);
    ui->Data_tree->setHeaderLabels( QStringList( { "Printers tree", "material index", "quality array index", "group array index" } ) );


}

void MainWindow::load_json_data_from_file()
{

    QFile data_file(current_data_file);
    if ( !data_file.open( QIODevice::ReadOnly ) )
    {
        ui->statusBar->showMessage( QString("Error open file %1 ").arg( current_data_file ) );
        return;
    }

    bool find_new_data = false;
    QByteArray text_data = data_file.readLine();
    if (text_data == "\r\n")
        text_data = data_file.readLine();

    while ( text_data.size() != 0 )
    {
        auto er = new QJsonParseError;
        QJsonDocument jsn_doc = QJsonDocument::fromJson(QByteArray::fromHex(text_data), er);
        if ( er->error == QJsonParseError::NoError )
        {
            if ( !find_new_data )
            {
                find_new_data = true;
                json_obj_vector.clear();
            }
            json_obj_vector.push_back(jsn_doc.object());
        }
        else
           qDebug() << "Parse error:" << er->errorString() << "offset" << er->offset;
        text_data = data_file.readLine();
    }

    data_file.close();

    if ( find_new_data )
    {
        tree_widget_filler();
        ui->statusBar->showMessage( QString("Loaded %1 materials").arg( json_obj_vector.size() ) );
    }
    else
        ui->statusBar->showMessage( QString("No data found in %1").arg( current_data_file ) );

}




void MainWindow::on_Apply_Button_clicked()
{
    auto current_item = current_treewidget_item;
    if ( current_item )
    {
        if ( ui->tabs->currentWidget() == ui->material_settings )
        {
            int json_material_index  = current_item->text( ColumIndex::JsonVectorIndex ).toInt();

            QJsonObject json_obj = json_obj_vector.at( json_material_index );
            foreach (QString key, json_obj.keys())
            {
                if ( json_obj[key].isDouble() )
                    if ( material_lineedit_holder.value(key) )
                    {
                        json_obj[key] = QJsonValue( material_lineedit_holder.value(key)->text().toDouble() );
                    }
                if ( json_obj[key].isString() )
                    if ( material_lineedit_holder.value(key) )
                    {
                        json_obj[key] = QJsonValue( material_lineedit_holder.value(key)->text() );
                    }
                if ( json_obj[key].isArray() )
                {
                    auto json_array = json_obj[key].toArray();
                    for (int elem_cnt = 0; elem_cnt < json_array.size(); ++elem_cnt)
                    {
                        if ( json_array[elem_cnt].isDouble() )
                            if ( material_lineedit_holder.value( key + QString(".%1").arg( elem_cnt ) ) )
                            {
                                json_array[elem_cnt] = QJsonValue( material_lineedit_holder.value( key + QString(".%1").arg( elem_cnt ) )->text().toDouble() );
                            }
                    }
                    json_obj[key] = json_array;
                }

            }
            json_obj_vector.at( json_material_index ) = json_obj;
            ui->Data_tree->topLevelItem( json_material_index )->setText( 0, material_lineedit_holder["b2"]->text() );
        }

        if ( ui->tabs->currentWidget() == ui->printer_settings )
        {
            int json_material_index  = current_item->text( ColumIndex::JsonVectorIndex ).toInt();
            int json_array_index     = current_item->text( ColumIndex::GroupArrayIndex ).toInt();

            QJsonObject json_obj = json_obj_vector.at( json_material_index );
            auto json_group_array_obj = json_obj.value("group").toArray();
            QJsonObject json_group_array_elem_obj = json_group_array_obj[ json_array_index ].toObject();

            foreach (QString key, json_group_array_elem_obj.keys())
            {
                if ( json_group_array_elem_obj[key].isDouble() )
                    if ( printer_lineedit_holder.value(key) )
                    {
                        json_group_array_elem_obj[key] = QJsonValue( printer_lineedit_holder.value(key)->text().toDouble() );
                    }
                if ( json_group_array_elem_obj[key].isString() )
                    if ( printer_lineedit_holder.value(key) )
                    {
                        json_group_array_elem_obj[key] = QJsonValue( printer_lineedit_holder.value(key)->text() );
                    }
                if ( json_group_array_elem_obj[key].isArray() )
                {
                    auto json_array = json_group_array_elem_obj[key].toArray();
                    for (int elem_cnt = 0; elem_cnt < json_array.size(); ++elem_cnt)
                    {
                        if ( json_array[elem_cnt].isDouble() )
                            if ( printer_lineedit_holder.value( key + QString(".%1").arg( elem_cnt ) ) )
                            {
                                json_array[elem_cnt] = QJsonValue( printer_lineedit_holder.value( key + QString(".%1").arg( elem_cnt ) )->text().toDouble() );
                            }
                    }
                    json_group_array_elem_obj[key] = json_array;
                }

            }
            json_group_array_obj[ json_array_index ] = json_group_array_elem_obj;
            json_obj["group"] = json_group_array_obj;
            json_obj_vector.at( json_material_index ) = json_obj;

            if ( current_item->parent() &&
                 current_item->parent()->parent() &&
                 current_item->parent()->parent()->parent() )
                if ( ( current_item->text(0) != printer_lineedit_holder["p4"]->text() ) ||
                     ( current_item->parent()->text(0) != printer_lineedit_holder["p3"]->text() ) ||
                     ( current_item->parent()->parent()->text(0) != printer_lineedit_holder["p2"]->text() ) ||
                     ( current_item->parent()->parent()->parent()->text(0) != printer_lineedit_holder["p1"]->text() ) )
                {
                    //place holder unrealize at treewidget
                    current_item->setBackground( 0, QBrush(Qt::darkYellow, Qt::SolidPattern ) );

                }

        }

    }


}

void MainWindow::on_Load_Button_clicked()
{
    current_data_file = QFileDialog::getOpenFileName( nullptr, tr("Open materials file"), "", tr("UPStudio materials files (*.fmd)") );
    if ( current_data_file.isEmpty() ) return;
    load_json_data_from_file();
}


void MainWindow::on_Save_Button_clicked()
{
    for (int rotate_step = amount_rotated_files; rotate_step > 0; --rotate_step)
    {
        QString rotate_file = current_data_file + QString(".%1").arg( rotate_step );
        QString rotate_plus_1_file = current_data_file + QString(".%1").arg( rotate_step + 1 );

        if ( QFile::exists( rotate_plus_1_file ) )
            if ( !QFile::remove( rotate_plus_1_file ) )
            {
                  ui->statusBar->showMessage( QString("Rotate files fail: can't remove %1").arg( rotate_plus_1_file ) );
                  return;
            }
        if ( QFile::exists( rotate_file ) )
            if ( !QFile::rename(rotate_file, rotate_plus_1_file ) )
            {
                  ui->statusBar->showMessage( QString("Rotate files fail: can't rename %1").arg( rotate_file ) );
                  return;
            }
    }
    if ( !QFile::rename(current_data_file, current_data_file + QString(".%1").arg( 1 ) ) )
    {
          ui->statusBar->showMessage( QString("Rotate files fail: can't rename %1").arg( current_data_file ) );
          return;
    }


    QFile save_data_file(current_data_file);
    if ( !save_data_file.open( QIODevice::WriteOnly | QIODevice::Truncate ) )
    {
          ui->statusBar->showMessage( QString("Rotate files fail: can't open %1").arg( current_data_file ) );
          return;
    }

    QTextStream file_stream(&save_data_file);

    for (QJsonObject json_obj : json_obj_vector)
    {
        QJsonDocument json_doc;
        json_doc.setObject(json_obj);
        file_stream << "\r\n" << json_doc.toJson(QJsonDocument::Compact).toHex();
    }

    file_stream << "\r\n";
    save_data_file.close();

    ui->statusBar->showMessage( QString("Data saved in %1").arg( current_data_file ) );
    QFile::remove( current_data_file + QString(".%1").arg( amount_rotated_files + 1 ) );
}

void MainWindow::on_Data_tree_currentItemChanged(QTreeWidgetItem *current, QTreeWidgetItem *previous)
{
    Q_UNUSED(previous);
    if ( current )
    {
        if ( ( current->text(ColumIndex::JsonVectorIndex ).size() ) && ( current->text(ColumIndex::GroupArrayIndex ).size() ) && ( current->text(ColumIndex::ArrayIndex ).size() ) )
        {
            QJsonObject json_obj = json_obj_vector.at( current->text( ColumIndex::JsonVectorIndex ).toInt() ).value("group").toArray()[ current->text( ColumIndex::GroupArrayIndex ).toInt() ].toObject();

            foreach (QString key, json_obj.keys())
            {
                if ( json_obj[key].isDouble() )
                    if ( printer_lineedit_holder.value(key) )
                    {
                         printer_lineedit_holder.value(key)->setText( qrealtoString_wo0( json_obj[key].toDouble() ) );
                    }
                if ( json_obj[key].isString() )
                    if ( printer_lineedit_holder.value(key) )
                    {
                         printer_lineedit_holder.value(key)->setText( json_obj[key].toString() );
                    }
                if ( json_obj[key].isArray() )
                    for (int elem_cnt = 0; elem_cnt < json_obj[key].toArray().size(); ++elem_cnt)
                    {
                        if ( json_obj[key].toArray()[elem_cnt].isDouble() )
                            if ( printer_lineedit_holder.value( key + QString(".%1").arg( elem_cnt ) ) )
                            {
                                 printer_lineedit_holder.value( key + QString(".%1").arg( elem_cnt ) )->setText( qrealtoString_wo0( json_obj[key].toArray()[elem_cnt].toDouble() ) );
                            }
                    }

            }

            ui->tabs->setCurrentWidget(ui->printer_settings);
            current_treewidget_item = current;
        }

        if ( ( current->parent() == nullptr ) && ( current->text(ColumIndex::JsonVectorIndex ).size() ) )
        {
            QJsonObject json_obj = json_obj_vector.at( current->text( ColumIndex::JsonVectorIndex ).toInt() );

            foreach (QString key, json_obj.keys())
            {
                if ( json_obj[key].isDouble() )
                    if ( material_lineedit_holder.value(key) )
                    {
                         material_lineedit_holder.value(key)->setText( qrealtoString_wo0( json_obj[key].toDouble() ) );
                    }
                if ( json_obj[key].isString() )
                    if ( material_lineedit_holder.value(key) )
                    {
                         material_lineedit_holder.value(key)->setText( json_obj[key].toString() );
                    }
                if ( json_obj[key].isArray() )
                    for (int elem_cnt = 0; elem_cnt < json_obj[key].toArray().size(); ++elem_cnt)
                    {
                        if ( json_obj[key].toArray()[elem_cnt].isDouble() )
                            if ( material_lineedit_holder.value( key + QString(".%1").arg( elem_cnt ) ) )
                            {
                                 material_lineedit_holder.value( key + QString(".%1").arg( elem_cnt ) )->setText( qrealtoString_wo0( json_obj[key].toArray()[elem_cnt].toDouble() ) );
                            }
                    }

            }

            ui->tabs->setCurrentWidget(ui->material_settings);
            current_treewidget_item = current;
        }
    }
}

void MainWindow::on_Load_def_user_Button_clicked()
{
    current_data_file = default_user_file;
    if ( current_data_file.isEmpty() ) return;
    load_json_data_from_file();
}

void MainWindow::on_Load_def_vendor_Button_clicked()
{
    current_data_file = default_vendor_file;
    if ( current_data_file.isEmpty() ) return;
    load_json_data_from_file();
}
