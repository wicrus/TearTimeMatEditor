#ifndef MISC_H
#define MISC_H

//#include <stdint.h>
#include <QObject>
#include <QEventLoop>
#include <QTimer>
#include <QDoubleValidator>
#include <QKeyEvent>
#include <QComboBox>
#include <QLineEdit>
#include <QLayout>
#include <QDebug>
#include <math.h>

inline void alignLayoutWidgets(QLayout *layout, Qt::Alignment alignment)
{
    int itemCount = layout->count();
    for(int i = 0; i < itemCount; ++i)
    {
        auto widget = layout->itemAt(i)->widget();
        layout->setAlignment(widget, alignment);
    }
}

class DeleteHighlightedItemWhenDelPressedEventFilter : public QObject
{
     Q_OBJECT
protected:
    bool eventFilter(QObject *obj, QEvent *event) override
    {
        if (event->type() == QEvent::KeyPress)
        {
            QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
            if (keyEvent->key() == Qt::Key::Key_Delete)
            {
                auto combobox = dynamic_cast<QComboBox *>(obj);
                if (combobox)
                {
                    combobox->removeItem(combobox->currentIndex());
                    return true;
                }
            }
        }
        return QObject::eventFilter(obj, event);
    }
};

class CustomDoubleValidator : public QDoubleValidator
{
public:
    CustomDoubleValidator(QObject *parent = nullptr) : QDoubleValidator(parent)
    {
        setNotation(QDoubleValidator::StandardNotation);
    }
    CustomDoubleValidator(double bottom, double top, int decimals, QObject *parent = nullptr) : QDoubleValidator(bottom, top, decimals, parent)
    {
        setNotation(QDoubleValidator::StandardNotation);
    }

    State validate(QString &str, int &pos) const
    {
        Q_UNUSED(pos);

        if(str.isEmpty()) return QValidator::Intermediate;

        if(str.contains(',')) return QValidator::Invalid;

        int indexOfDecimalPoint = str.indexOf('.');
        if(indexOfDecimalPoint != -1)
        {
            int charsAfterPoint = str.length() - indexOfDecimalPoint - 1;
            if (charsAfterPoint > decimals()) return QValidator::Invalid;
        }

        QString s(str);
        s.replace('.', locale().decimalPoint());

        bool ok;
        double value = locale().toDouble(s, &ok);

        if(ok && (qFuzzyCompare(value, bottom()) || value > bottom()) && (qFuzzyCompare(value, top()) || value < top()))
            return QValidator::Acceptable;
        else return QValidator::Invalid;
    }
};

inline bool validateAndSetText(QLineEdit *lineEdit, QString text)
{
    QValidator *validator = const_cast<QValidator *>(lineEdit->validator());

    if(validator)
    {
        int pos;
        if(!(validator->validate(text, pos) == QValidator::Acceptable)) return false;
    }

    lineEdit->setText(text);
    return true;
}

template<typename Func>
bool waitSignal(const typename QtPrivate::FunctionPointer<Func>::Object *sender, Func signal, int timeout = 30000)
{
    QEventLoop loop;
    QTimer timer;
    timer.setSingleShot(true);
    QObject::connect(sender, signal, &loop, &QEventLoop::quit);
    QObject::connect(&timer, &QTimer::timeout, &loop, &QEventLoop::quit);
    timer.start(timeout);
    loop.exec();
    if(timer.isActive()) return true;
    else return false;
}

template<typename Signal, typename Func>
bool waitSignalAfterFunction(const typename QtPrivate::FunctionPointer<Signal>::Object *sender, Signal signal, Func function, int timeout = 30000)
{
    QEventLoop loop;
    QTimer timer;
    timer.setSingleShot(true);
    QObject::connect(sender, signal, &loop, &QEventLoop::quit);
    QObject::connect(&timer, &QTimer::timeout, &loop, &QEventLoop::quit);
    function();
    timer.start(timeout);
    loop.exec();
    if(timer.isActive()) return true;
    else return false;
}

inline QString qrealtoString_wo0( qreal num )
{
    QString str = QString::number( num, 'f', 5 );

    str.remove( QRegExp("0+$") ); // Remove any number of trailing 0's
    str.remove( QRegExp("\\.$") ); // If the last character is just a '.' then remove it

    return str;
}

#endif // MISC_H


